package com.gobrs.async.test.task.retry;

import lombok.Data;

/**
 * @program: gobrs-async
 * @ClassName DataContentVo
 * @description:
 * @author: sizegang
 * @create: 2023-02-23
 **/
@Data
public class DataContentVo {
    private Object aResult;

    private Object bResult;

}
