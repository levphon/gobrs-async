package com.gobrs.async.test.task.retry;

import com.gobrs.async.core.TaskSupport;
import com.gobrs.async.core.anno.Task;
import com.gobrs.async.core.task.AsyncTask;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

/**
 * @program: gobrs-async
 * @ClassName GobrsTaskA
 * @description:
 * @author: sizegang
 * @create: 2022-10-31
 **/
@Slf4j
@Task
public class CaseRetryTaskA extends AsyncTask<DataContentVo, Object> {

    @Override
    public void prepare(DataContentVo o) {
        log.info(this.getName() + " 使用线程---" + Thread.currentThread().getName());
    }

    @SneakyThrows
    @Override
    public Object task(DataContentVo o, TaskSupport support) {
        System.out.println("CaseRetryTaskA Begin");
        System.out.println("CaseRetryTaskA End");
        o.setAResult(new byte[1024 * 1024]);
        return o;
    }
}
